import { Component, OnInit, ViewChild } from '@angular/core';
import { HelperService } from '../services/helper.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  @ViewChild('email', { static: true }) email;
  @ViewChild('password', { static: true }) password;
  error;
  ishidden: boolean;

  constructor(public helper: HelperService) { }

  ngOnInit() {
  	this.ishidden = true;
  }

  login(){
  	this.error = '';
  	if (this.email.value == '') {
     this.error = "Please enter your email address";
      return;
    }
    else if (this.password.value.length < 6) {
      this.error = "Password must have at least 6 characters";
      return;
    }
    else if (this.password.value == '') {
      this.error = "Please enter your password";
      return;
    }
    else {
    	this.ishidden = false;
    	this.helper.login(this.email.value, this.password.value);
    	this.ishidden = true;
    }
  }

}
