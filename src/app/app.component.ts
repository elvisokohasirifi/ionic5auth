import { Component, ViewChild } from '@angular/core';

import { Platform, IonRouterOutlet, NavController, AlertController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';

import { AngularFireAuth } from "@angular/fire/auth";

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  @ViewChild(IonRouterOutlet, {static: false}) routerOutlet: IonRouterOutlet;
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private nav: NavController,
    private router: Router,
    private storage: Storage,
    private alertController: AlertController,
    public ngFireAuth: AngularFireAuth
  ) {
    this.initializeApp();
    this.platform.backButton.subscribe(() => {
      if (this.routerOutlet && this.routerOutlet.canGoBack()) {
        this.routerOutlet.pop();
      } else {
        this.confirmClose();
      }
    });
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      let a = this.nav;
      this.ngFireAuth.onAuthStateChanged(function(user) {
        if (user) {
          a.navigateRoot('tabs/tab1');
        } else {
          a.navigateRoot('welcome');
        }
      });
    });
  }

  async confirmClose(){
    const alert = await this.alertController.create({
        message: 'Are you sure you want to close this app?',
        buttons: [
          {
            text: 'No',
            role: 'cancel',
            cssClass: 'dark',
            handler: (blah) => {
              //console.log('Confirm Cancel: blah');
            }
          }, {
            text: 'Yes',
            handler: () => {
              navigator['app'].exitApp();
            }
          }
        ]
      });

      await alert.present();
  }
}