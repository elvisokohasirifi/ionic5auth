import { Injectable } from '@angular/core';
import { NavController, ToastController, LoadingController, AlertController, Platform } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { AngularFireAuth } from "@angular/fire/auth";
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class HelperService {
	name;
  email;
  phone;
  userid;

  constructor(private nav: NavController, 
  						private alertController: AlertController, 
  						private storage: Storage, 
  						public afStore: AngularFirestore,
    					public ngFireAuth: AngularFireAuth) {

  	// read values from local storage
    Promise.all([this.storage.get("name"), this.storage.get("email"), this.storage.get("phone"), this.storage.get("userid")]).then(values => {    
      this.name = values[0];
      this.email = values[1];
      this.phone = values[2];
			this.userid = values[3];
    });
  }

  login(email, password){
  	this.ngFireAuth.signInWithEmailAndPassword(email, password).then((res) => {
      var docRef = this.afStore.collection('users').doc(res.user.uid);
      this.save('userid', res.user.uid);
      docRef.get().toPromise().then(doc => {
        if (doc.exists) {
           this.saveandcontinue(doc);
        } else {
            alert('We do not have your details');
        }
      }).catch(function(error) {
          alert("Error getting document: " + error);
      });
    }).catch((error) => {
      alert(error.message);
    });
  }

  saveandcontinue(doc){
    this.save('email', doc.data().email);
    this.save('name', doc.data().name);
    this.save('phone', doc.data().phone);
    this.save('role', doc.data().role);
    this.save('loggedin', true);
    this.home('/tabs/tab1');
  }

  signup(name, email, phone, password){
  	this.ngFireAuth.createUserWithEmailAndPassword(email, password).then((res) => {
      var docRef = this.afStore.collection('users').doc(res.user.uid);
      docRef.get().toPromise().then(doc => {
        if (!doc.exists) {
          this.addNewUserToFirestore(res, name, phone);
        }
      })
     .catch(error => {
          alert('Checking if customer exists failed" ' + error);
          console.log(error);
          return;
      });
      alert('Sign up successful. Log in to continue');
      this.home('login');
    }).catch((error) => {
      alert(error.message);
    });
  }

  addNewUserToFirestore(res, name, phone) {
    this.afStore.collection('users').doc(res.user.uid)
      .set({
          name: name,
          phone: phone,
          email: res.user.email
      })
      //ensure to catch any errors at this stage to advise us if something does go wrong
      .catch(error => {
          alert('Something went wrong with added user to firestore: ' + JSON.stringify(error));
          return;
      });
  }

  forgotpassword(email){
  	this.ngFireAuth.sendPasswordResetEmail(email).then(() => {
  			alert('Password Reset Link Sent');
  		})
      .catch((error) => {
        alert(error.message);
      });
  }

  /*changepassword(newPassword){
  	var user = this.ngFireAuth.currentUser;
		user.updatePassword(newPassword).then(function() {
		  alert('Password updated successfully');
		}).catch(function(error) {
		  alert(error.message);
		});
  }*/

  navigate(url){
  	this.nav.navigateForward(url);
  }

  back(){
  	this.nav.pop();
  }

  home(url){
    this.nav.navigateRoot(url);
  }

  async presentAlert(title, message) {
    const alert = await this.alertController.create({
      subHeader: title,
      message: message,
      buttons: ['OK']
    });

    await alert.present();
  }

  save(key, value){
    this.storage.set(key, value);
  }
  
  async get(key){
    let value = null;
    await this.storage.get(key).then((val) => {
      value = val;
    });
    return value;
  }

  clear(){
    this.storage.clear();
    this.name = null;
    this.email = null;
    this.userid = null;
    this.phone = null;
  }

}
