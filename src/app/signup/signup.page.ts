import { Component, OnInit, ViewChild } from '@angular/core';
import { HelperService } from '../services/helper.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {

  @ViewChild('name', { static: true }) name;
  @ViewChild('email', { static: true }) email;
  @ViewChild('phone', { static: true }) phone;
  @ViewChild('password', { static: true }) password;
  error;
  ishidden: boolean;

  constructor(public helper: HelperService) { }

  ngOnInit() {
  	this.ishidden = true;
  }

  signup(){
  	this.error = '';
  	if (this.name.value == '') {
      this.error = 'Please enter a valid name';
      return;
    }
    else if (this.name.value.length < 3 || !this.name.value.includes(' ')) {
      this.error = 'Please enter your full name';
      return;
    }   
    else if (this.email.value == '') {
      this.error = 'Please enter a valid email';
      return;
    }
    else if (this.phone.value == null) {
      this.error = 'Please enter a valid phone number';
      return;
    }
    else if (this.password.value == '') {
      this.error = 'Please enter a valid password';
      return;
    }
    else if (this.password.value.length < 6) {
      this.error = 'Password must have at least 6 characters';
      return;
    }
    else {
	    this.helper.signup(this.name.value, this.email.value, this.phone.value, this.password.value);
	}
  }


}
