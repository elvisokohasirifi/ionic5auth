import { Component, OnInit, ViewChild } from '@angular/core';
import { HelperService } from '../services/helper.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.page.html',
  styleUrls: ['./forgot-password.page.scss'],
})
export class ForgotPasswordPage implements OnInit {
  @ViewChild('email', { static: true }) email;
  ishidden: boolean;
  error;

  constructor(public helper: HelperService) { }

  ngOnInit() {
  	this.ishidden = true;
  }

  submit(){
  	this.error = '';
  	if (this.email.value == '') {
     this.error = "Please enter your email address";
      return;
    }
    this.helper.forgotpassword(this.email.value);
  }

}
